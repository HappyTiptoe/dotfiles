if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

###########
# history #
###########

# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# set history size
HISTSIZE=1000
HISTFILESIZE=2000

###########
# colours #
###########

PS1='[\[\033[01;35m\]\u@\h\[\033[00m\] \W]\$ '
export LS_COLORS=$LS_COLORS:"di=1;35:"


###########
# aliases #
###########

alias off="shutdown -P 1"

alias cd..="cd .."
alias clip="xclip -selection clipboard <"
alias clone="git clone"
alias pls="sudo"
alias rmrf="rm -rf"

alias s="nr start"
alias d="nr dev"
alias b="nr build"

alias gi="git init"
alias ga="git add"
alias gaa="git add ."
alias gc="git commit -m"
alias gp="git push"
alias gs="git status"


#############
# functions #
#############

function c() {
  cd ~/c/$1
}

function m() {
  cd ~/m/$1
}

function dir() {
  mkdir $1 && cd $1
}

function kp() {
  fuser -k $1/tcp
  echo "Killing process on port $1..."
}

# check if npm package name is available
function npmc() {
  node ~/c/_/npm_name_checker/index.js $1
}
